---
swagger: "2.0"
info:
  description: "Spring Boot BDP REST API Manual"
  version: "1.0"
  title: "Spring Boot BDP REST API Manual"
  termsOfService: "Terms of service"
  contact:
    name: "Vulcano"
    url: "http://www.bennu.cl"
  license:
    name: "Apache License Version 2.0"
    url: "https://www.apache.org/licenses/LICENSE-2.0"
host: "localhost:8080"
basePath: "/"
tags:
- name: "bdp-persona-controller"
  description: "Bdp Persona Controller"
paths:
  /personas:
    post:
      tags:
      - "bdp-persona-controller"
      summary: "inserta datos de una persona"
      operationId: "insertarPersonaUsingPOST"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters:
      - in: "body"
        name: "persona"
        description: "persona nueva"
        required: true
        schema:
          $ref: "#/definitions/PersonaInsertar"
      responses:
        201:
          description: "Persona creada"
          schema:
            $ref: "#/definitions/Persona"
        400:
          description: "Faltan datos de la persona"
        401:
          description: "Unauthorized"
        403:
          description: "Forbidden"
        500:
          description: "Error interno al insertar Persona"
      x-swagger-router-controller: "BdpPersonaController"
  /personas/{rut}:
    get:
      tags:
      - "bdp-persona-controller"
      summary: "Obtener datos de persona segun rut indicado"
      operationId: "obtenerPersonaUsingGET"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters:
      - name: "rut"
        in: "path"
        description: "rut de la persona"
        required: true
        type: "integer"
        format: "int32"
      responses:
        200:
          description: "Persona encontrada"
          schema:
            $ref: "#/definitions/Persona"
        401:
          description: "Unauthorized"
        403:
          description: "Forbidden"
        404:
          description: "Persona no encontrada"
        500:
          description: "Error interno al intentar obtener Persona"
      x-swagger-router-controller: "BdpPersonaController"
  /personas/{rut}/direcciones:
    get:
      tags:
      - "bdp-persona-controller"
      summary: "Obtiene direccones asociadas a una persona"
      operationId: "getDireccionesPersonaUsingGET"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters:
      - name: "rut"
        in: "path"
        description: "rut de la persona"
        required: true
        type: "integer"
        format: "int32"
      responses:
        200:
          description: "Direcciones encontradas"
          schema:
            type: "array"
            items:
              $ref: "#/definitions/Direccion"
        401:
          description: "Unauthorized"
        403:
          description: "Forbidden"
        404:
          description: "Direcciones no encontradas"
        500:
          description: "Error interno al obtener direcciones"
      x-swagger-router-controller: "BdpPersonaController"
    post:
      tags:
      - "bdp-persona-controller"
      summary: "Inserta la dirección de una persona"
      operationId: "guardarDireccionUsingPOST"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters:
      - name: "rut"
        in: "path"
        description: "rut de la persona"
        required: true
        type: "integer"
        format: "int32"
      - in: "body"
        name: "direccion"
        description: "direccion de la persona"
        required: true
        schema:
          $ref: "#/definitions/Direccion"
      responses:
        201:
          description: "Direccion creada"
          schema:
            $ref: "#/definitions/Direccion"
        400:
          description: "Faltan datos de la dirección"
        401:
          description: "Unauthorized"
        403:
          description: "Forbidden"
        500:
          description: "Error interno al agregar direccion"
      x-swagger-router-controller: "BdpPersonaController"
  /personas/{rut}/polizas:
    get:
      tags:
      - "bdp-persona-controller"
      summary: "obtener Polizas de persona segun rut indicado"
      operationId: "obtenerPolizasPersonaUsingGET"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters:
      - name: "rut"
        in: "path"
        description: "rut"
        required: true
        type: "integer"
        format: "int32"
      responses:
        200:
          description: "Polizas encontradas"
          schema:
            type: "array"
            items:
              $ref: "#/definitions/Poliza"
        401:
          description: "Unauthorized"
        403:
          description: "Forbidden"
        404:
          description: "Polizas no encontraos"
        500:
          description: "Error interno al obtener polizas"
      x-swagger-router-controller: "BdpPersonaController"
    post:
      tags:
      - "bdp-persona-controller"
      summary: "inserta una poliza de una persona"
      operationId: "insertarContratoUsingPOST"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters:
      - in: "body"
        name: "poliza"
        description: "nueva poliza"
        required: true
        schema:
          $ref: "#/definitions/PolizaInsertar"
      - name: "rut"
        in: "path"
        description: "rut"
        required: true
        type: "integer"
        format: "int32"
      responses:
        201:
          description: "Poliza creada"
          schema:
            $ref: "#/definitions/Poliza"
        400:
          description: "Faltan datos de la poliza"
        401:
          description: "Unauthorized"
        403:
          description: "Forbidden"
        500:
          description: "Error interno al insertar Poliza"
      x-swagger-router-controller: "BdpPersonaController"
  /personas/{rut}/polizas/{numero_poliza}:
    get:
      tags:
      - "bdp-persona-controller"
      summary: "Obtener datos de poliza segun rut indicado y numero de poliza"
      operationId: "obtenerPolizaPersonaUsingGET"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters:
      - name: "rut"
        in: "path"
        description: "rut de la persona"
        required: true
        type: "integer"
        format: "int32"
      - name: "numero_poliza"
        in: "path"
        description: "numero de poliza"
        required: true
        type: "string"
      responses:
        200:
          description: "Polizas encontradas"
          schema:
            $ref: "#/definitions/Poliza"
        401:
          description: "Unauthorized"
        403:
          description: "Forbidden"
        404:
          description: "Polizas no encontraos"
        500:
          description: "Error interno al obtener polizas"
      x-swagger-router-controller: "BdpPersonaController"
  /personas/{rut}/roles:
    get:
      tags:
      - "bdp-persona-controller"
      summary: "Obtener contratos y roles de persona segun rut indicado"
      operationId: "obtenerContratosRolesPersonaUsingGET"
      consumes:
      - "application/json"
      produces:
      - "application/json"
      parameters:
      - name: "rut"
        in: "path"
        description: "rut de la persona"
        required: true
        type: "integer"
        format: "int32"
      - name: "negocioId"
        in: "query"
        description: "tipo de negocio"
        required: false
        type: "string"
      - name: "numeroPoliza"
        in: "query"
        description: "numero de poliza"
        required: false
        type: "string"
      responses:
        200:
          description: "Contratos y roles encontrados"
          schema:
            type: "array"
            items:
              $ref: "#/definitions/Rol"
        401:
          description: "Unauthorized"
        403:
          description: "Forbidden"
        404:
          description: "Contratos y roles no encontraos"
        500:
          description: "Error interno al obtener Contratos y roles"
      x-swagger-router-controller: "BdpPersonaController"
definitions:
  Persona:
    type: "object"
    properties:
      rut:
        type: "integer"
        format: "int32"
      rut_dv:
        type: "string"
      nombre:
        type: "string"
      apellido_paterno:
        type: "string"
      apellido_materno:
        type: "string"
      email:
        type: "string"
      fecha_nacimiento:
        type: "integer"
        format: "int32"
      fecha_defuncion:
        type: "integer"
        format: "int32"
      celular:
        type: "string"
      tipo_persona:
        type: "string"
      estado_civil_id:
        type: "string"
      pais_id:
        type: "string"
      genero_id:
        type: "string"
      afp_id:
        type: "string"
      cargo_id:
        type: "string"
      isapre_id:
        type: "string"
      nivel_educacion_id:
        type: "string"
      nivel_ingreso_id:
        type: "string"
      profesion_id:
        type: "string"
      rubro_id:
        type: "string"
    example:
      pais_id: "pais_id"
      cargo_id: "cargo_id"
      isapre_id: "isapre_id"
      fecha_nacimiento: 6
      genero_id: "genero_id"
      rubro_id: "rubro_id"
      estado_civil_id: "estado_civil_id"
      nombre: "nombre"
      rut_dv: "rut_dv"
      rut: 0
      profesion_id: "profesion_id"
      nivel_ingreso_id: "nivel_ingreso_id"
      apellido_paterno: "apellido_paterno"
      apellido_materno: "apellido_materno"
      tipo_persona: "tipo_persona"
      afp_id: "afp_id"
      celular: "celular"
      fecha_defuncion: 1
      nivel_educacion_id: "nivel_educacion_id"
      email: "email"
  Poliza:
    type: "object"
    properties:
      numero_poliza:
        type: "string"
      inicio_vigencia:
        type: "integer"
        format: "int32"
      termino_vigencia:
        type: "integer"
        format: "int32"
      negocio_id:
        type: "string"
      producto_id:
        type: "string"
      estado_poliza_id:
        type: "string"
      sucursal_venta_id:
        type: "string"
      sucursal_mantencion_id:
        type: "string"
      forma_pago_id:
        type: "string"
    example:
      numero_poliza: "numero_poliza"
      negocio_id: "negocio_id"
      sucursal_mantencion_id: "sucursal_mantencion_id"
      estado_poliza_id: "estado_poliza_id"
      producto_id: "producto_id"
      sucursal_venta_id: "sucursal_venta_id"
      forma_pago_id: "forma_pago_id"
      termino_vigencia: 6
      inicio_vigencia: 0
  Direccion:
    type: "object"
    properties:
      cale:
        type: "string"
      telefono:
        type: "string"
      comunda_id:
        type: "string"
      pais_id:
        type: "string"
    example:
      pais_id: "pais_id"
      cale: "cale"
      telefono: "telefono"
      comunda_id: "comunda_id"
  Rol:
    type: "object"
    properties:
      poliza_id:
        type: "string"
      inicio_vigencia:
        type: "integer"
        format: "int64"
      tipo_negocio_id:
        type: "string"
      tipo_producto_id:
        type: "string"
    example:
      tipo_producto_id: "tipo_producto_id"
      poliza_id: "poliza_id"
      tipo_negocio_id: "tipo_negocio_id"
      inicio_vigencia: 0
  PersonaInsertar:
    type: "object"
    properties:
      rut:
        type: "integer"
        format: "integer32"
      rut_dv:
        type: "string"
      apellido_materno:
        type: "string"
      apellido_paterno:
        type: "string"
      fecha_nacimiento:
        type: "integer"
        format: "integer32"
    example:
      rut: 0
      apellido_materno: "apellido_materno"
      apellido_paterno: "apellido_paterno"
      fecha_nacimiento: 6
      rut_dv: "rut_dv"
  PolizaInsertar:
    type: "object"
    properties:
      numero_poliza:
        type: "string"
      negocio_id:
        type: "string"
      producto_id:
        type: "string"
      estado_poliza_id:
        type: "string"
      sucursal_venta_id:
        type: "string"
      forma_pago_id:
        type: "string"
    example:
      numero_poliza: "numero_poliza"
      negocio_id: "negocio_id"
      estado_poliza_id: "estado_poliza_id"
      producto_id: "producto_id"
      sucursal_venta_id: "sucursal_venta_id"
      forma_pago_id: "forma_pago_id"
