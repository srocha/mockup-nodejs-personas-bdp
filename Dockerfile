# Do the npm install or yarn install in the full image
FROM mhart/alpine-node:8
WORKDIR /app
COPY package.json yarn.lock ./
RUN yarn prestart
COPY . .

EXPOSE 8080

CMD ["yarn", "start"]
