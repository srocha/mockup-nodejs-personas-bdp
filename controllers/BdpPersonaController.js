'use strict';

var utils = require('../utils/writer.js');
var BdpPersonaController = require('../service/BdpPersonaControllerService');

module.exports.getDireccionesPersonaUsingGET = function getDireccionesPersonaUsingGET (req, res, next) {
  var rut = req.swagger.params['rut'].value;
  BdpPersonaController.getDireccionesPersonaUsingGET(rut)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.guardarDireccionUsingPOST = function guardarDireccionUsingPOST (req, res, next) {
  var rut = req.swagger.params['rut'].value;
  var direccion = req.swagger.params['direccion'].value;
  BdpPersonaController.guardarDireccionUsingPOST(rut,direccion)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.insertarContratoUsingPOST = function insertarContratoUsingPOST (req, res, next) {
  var poliza = req.swagger.params['poliza'].value;
  var rut = req.swagger.params['rut'].value;
  BdpPersonaController.insertarContratoUsingPOST(poliza,rut)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.insertarPersonaUsingPOST = function insertarPersonaUsingPOST (req, res, next) {
  var persona = req.swagger.params['persona'].value;
  BdpPersonaController.insertarPersonaUsingPOST(persona)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.obtenerContratosRolesPersonaUsingGET = function obtenerContratosRolesPersonaUsingGET (req, res, next) {
  var rut = req.swagger.params['rut'].value;
  var negocioId = req.swagger.params['negocioId'].value;
  var numeroPoliza = req.swagger.params['numeroPoliza'].value;
  BdpPersonaController.obtenerContratosRolesPersonaUsingGET(rut,negocioId,numeroPoliza)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.obtenerPersonaUsingGET = function obtenerPersonaUsingGET (req, res, next) {
  var rut = req.swagger.params['rut'].value;
  BdpPersonaController.obtenerPersonaUsingGET(rut)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.obtenerPolizaPersonaUsingGET = function obtenerPolizaPersonaUsingGET (req, res, next) {
  var rut = req.swagger.params['rut'].value;
  var numero_poliza = req.swagger.params['numero_poliza'].value;
  BdpPersonaController.obtenerPolizaPersonaUsingGET(rut,numero_poliza)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.obtenerPolizasPersonaUsingGET = function obtenerPolizasPersonaUsingGET (req, res, next) {
  var rut = req.swagger.params['rut'].value;
  BdpPersonaController.obtenerPolizasPersonaUsingGET(rut)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};
