'use strict';


/**
 * Obtiene direccones asociadas a una persona
 *
 * rut Integer rut de la persona
 * returns List
 **/
exports.getDireccionesPersonaUsingGET = function (rut) {
    return new Promise(function (resolve, reject) {
        var examples = {};
        if (rut === 1) {
            examples['application/json'] = [
                {
                    "calle": "VALENCIA  2125",
                    "telefono": null,
                    "comunda_id": "11245",
                    "pais_id": null
                }
            ];
        } else if (rut === 2665352) {
            examples['application/json'] = [
                {
                    "calle": "ESCANDINAVIA 110 DPTO 804",
                    "telefono": null,
                    "comunda_id": "11244",
                    "pais_id": "25"
                }
            ]
        }
        if (Object.keys(examples).length > 0) {
            resolve(examples[Object.keys(examples)[0]]);
        } else {
            resolve();
        }
    });
}


/**
 * Inserta la dirección de una persona
 *
 * rut Integer rut de la persona
 * direccion Direccion direccion de la persona
 * returns Direccion
 **/
exports.guardarDireccionUsingPOST = function (rut, direccion) {
    return new Promise(function (resolve, reject) {
        var examples = {};
        examples['application/json'] = {
            "pais_id": "pais_id",
            "cale": "cale",
            "telefono": "telefono",
            "comunda_id": "comunda_id"
        };
        if (Object.keys(examples).length > 0) {
            resolve(examples[Object.keys(examples)[0]]);
        } else {
            resolve();
        }
    });
}


/**
 * inserta una poliza de una persona
 *
 * poliza PolizaInsertar nueva poliza
 * rut Integer rut
 * returns Poliza
 **/
exports.insertarContratoUsingPOST = function (poliza, rut) {
    return new Promise(function (resolve, reject) {
        var examples = {};
        examples['application/json'] = {
            "numero_poliza": "numero_poliza",
            "negocio_id": "negocio_id",
            "sucursal_mantencion_id": "sucursal_mantencion_id",
            "estado_poliza_id": "estado_poliza_id",
            "producto_id": "producto_id",
            "sucursal_venta_id": "sucursal_venta_id",
            "forma_pago_id": "forma_pago_id",
            "termino_vigencia": 6,
            "inicio_vigencia": 0
        };
        if (Object.keys(examples).length > 0) {
            resolve(examples[Object.keys(examples)[0]]);
        } else {
            resolve();
        }
    });
}


/**
 * inserta datos de una persona
 *
 * persona PersonaInsertar persona nueva
 * returns Persona
 **/
exports.insertarPersonaUsingPOST = function (persona) {
    return new Promise(function (resolve, reject) {
        var examples = {};
        examples['application/json'] = {
            "pais_id": "pais_id",
            "cargo_id": "cargo_id",
            "isapre_id": "isapre_id",
            "fecha_nacimiento": 6,
            "genero_id": "genero_id",
            "rubro_id": "rubro_id",
            "estado_civil_id": "estado_civil_id",
            "nombre": "nombre",
            "rut_dv": "rut_dv",
            "rut": 0,
            "profesion_id": "profesion_id",
            "nivel_ingreso_id": "nivel_ingreso_id",
            "apellido_paterno": "apellido_paterno",
            "apellido_materno": "apellido_materno",
            "tipo_persona": "tipo_persona",
            "afp_id": "afp_id",
            "celular": "celular",
            "fecha_defuncion": 1,
            "nivel_educacion_id": "nivel_educacion_id",
            "email": "email"
        };
        if (Object.keys(examples).length > 0) {
            resolve(examples[Object.keys(examples)[0]]);
        } else {
            resolve();
        }
    });
}


/**
 * Obtener contratos y roles de persona segun rut indicado
 *
 * rut Integer rut de la persona
 * negocioId String tipo de negocio (optional)
 * numeroPoliza String numero de poliza (optional)
 * returns List
 **/
exports.obtenerContratosRolesPersonaUsingGET = function (rut, negocioId, numeroPoliza) {
    return new Promise(function (resolve, reject) {
        var examples = {};
        if (rut === 1) {
            examples['application/json'] = [
                {
                    "numero_poliza": "15607",
                    "inicio_vigencia": null,
                    "termino_vigencia": 1301540400000,
                    "negocio_id": "CREIND",
                    "producto_id": "20050",
                    "estado_poliza_id": "503",
                    "sucursal_venta_id": "81",
                    "sucursal_mantencion_id": null,
                    "forma_pago_id": null
                },
                {
                    "numero_poliza": "18710",
                    "inicio_vigencia": null,
                    "termino_vigencia": 1180584000000,
                    "negocio_id": "CREIND",
                    "producto_id": "20050",
                    "estado_poliza_id": "503",
                    "sucursal_venta_id": "306",
                    "sucursal_mantencion_id": null,
                    "forma_pago_id": null
                },
                {
                    "numero_poliza": "8440",
                    "inicio_vigencia": null,
                    "termino_vigencia": 1254283200000,
                    "negocio_id": "CREIND",
                    "producto_id": "20050",
                    "estado_poliza_id": "503",
                    "sucursal_venta_id": "133",
                    "sucursal_mantencion_id": null,
                    "forma_pago_id": null
                },
                {
                    "numero_poliza": "32026",
                    "inicio_vigencia": null,
                    "termino_vigencia": 1314759600000,
                    "negocio_id": "CREIND",
                    "producto_id": "20050",
                    "estado_poliza_id": "503",
                    "sucursal_venta_id": "133",
                    "sucursal_mantencion_id": null,
                    "forma_pago_id": null
                },
                {
                    "numero_poliza": "31241",
                    "inicio_vigencia": null,
                    "termino_vigencia": 1212206400000,
                    "negocio_id": "CREIND",
                    "producto_id": "20050",
                    "estado_poliza_id": "503",
                    "sucursal_venta_id": "307",
                    "sucursal_mantencion_id": null,
                    "forma_pago_id": null
                }
            ]
        }
        if (rut === 2665352) {
            examples['application/json'] = [
                {
                    "numero_poliza": "90102",
                    "inicio_vigencia": 1125460800000,
                    "termino_vigencia": 1286164800000,
                    "negocio_id": "RENPRIV",
                    "producto_id": "60001",
                    "estado_poliza_id": "4",
                    "sucursal_venta_id": "51",
                    "sucursal_mantencion_id": null,
                    "forma_pago_id": null
                },
                {
                    "numero_poliza": "9-1590300",
                    "inicio_vigencia": 902462400000,
                    "termino_vigencia": 925444800000,
                    "negocio_id": "SEGIND",
                    "producto_id": "200",
                    "estado_poliza_id": "401",
                    "sucursal_venta_id": "131",
                    "sucursal_mantencion_id": null,
                    "forma_pago_id": "3"
                }
            ];
        }
        if (Object.keys(examples).length > 0) {
            resolve(examples[Object.keys(examples)[0]]);
        } else {
            resolve();
        }
    });
}


/**
 * Obtener datos de persona segun rut indicado
 *
 * rut Integer rut de la persona
 * returns Persona
 **/
exports.obtenerPersonaUsingGET = function (rut) {
    return new Promise(function (resolve, reject) {
        var examples = {};
        if (rut === 1) {
            examples['application/json'] = {
                "rut": 1,
                "rut_dv": "9",
                "nombre": "LEGALES",
                "apellido_paterno": "HEREDEROS",
                "apellido_materno": ".",
                "email": "test.sucursal@bicevida.cl",
                "fecha_nacimiento": 1425956400000,
                "fecha_defuncion": 1358218800000,
                "celular": null,
                "tipo_persona": "PERSONA_NATURAL",
                "estado_civil_id": "0",
                "pais_id": "77",
                "genero_id": "1",
                "afp_id": "0",
                "cargo_id": null,
                "isapre_id": "0",
                "nivel_educacion_id": "0",
                "nivel_ingreso_id": null,
                "profesion_id": null,
                "rubro_id": "0"
            }
        } else if (rut === 2665352) {
            examples['application/json'] = {
                "rut": 2665352,
                "rut_dv": "5",
                "nombre": "CARLOS ALBERTO",
                "apellido_paterno": "ARACENA",
                "apellido_materno": "TRONCI",
                "email": null,
                "fecha_nacimiento": -1178049600000,
                "fecha_defuncion": 1286164800000,
                "celular": null,
                "tipo_persona": "PERSONA_NATURAL",
                "estado_civil_id": "4",
                "pais_id": "25",
                "genero_id": "1",
                "afp_id": null,
                "cargo_id": "0",
                "isapre_id": null,
                "nivel_educacion_id": "0",
                "nivel_ingreso_id": "0",
                "profesion_id": null,
                "rubro_id": "0"
            }
        } else {
            //NOT FOUND
            examples['application/json'] = {
                "code": 404,
                "error": "not_found",
                "error_description": "Datos no encontrados"
            }
        }
        if (Object.keys(examples).length > 0) {
            resolve(examples[Object.keys(examples)[0]]);
        } else {
            resolve();
        }
    });
}


/**
 * Obtener datos de poliza segun rut indicado y numero de poliza
 *
 * rut Integer rut de la persona
 * numero_poliza String numero de poliza
 * returns Poliza
 **/
exports.obtenerPolizaPersonaUsingGET = function (rut, numero_poliza) {
    return new Promise(function (resolve, reject) {
        var examples = {};
        if (rut === 1) {
            if (numero_poliza === "15607") {
                examples['application/json'] = {
                    "numero_poliza": "15607",
                    "inicio_vigencia": null,
                    "termino_vigencia": 1301540400000,
                    "negocio_id": "CREIND",
                    "producto_id": "20050",
                    "estado_poliza_id": "503",
                    "sucursal_venta_id": "81",
                    "sucursal_mantencion_id": null,
                    "forma_pago_id": null
                };
            } else if (numero_poliza === "18710") {
                examples['application/json'] = {
                    "numero_poliza": "18710",
                    "inicio_vigencia": null,
                    "termino_vigencia": 1180584000000,
                    "negocio_id": "CREIND",
                    "producto_id": "20050",
                    "estado_poliza_id": "503",
                    "sucursal_venta_id": "306",
                    "sucursal_mantencion_id": null,
                    "forma_pago_id": null
                }
            } else if (numero_poliza === "8440") {
                examples['application/json'] = {
                    "numero_poliza": "8440",
                    "inicio_vigencia": null,
                    "termino_vigencia": 1254283200000,
                    "negocio_id": "CREIND",
                    "producto_id": "20050",
                    "estado_poliza_id": "503",
                    "sucursal_venta_id": "133",
                    "sucursal_mantencion_id": null,
                    "forma_pago_id": null
                }
            }
        } else if (rut === 2665352) {
            if (numero_poliza === "90102") {
                examples['application/json'] = {
                    "numero_poliza": "90102",
                    "inicio_vigencia": 1125460800000,
                    "termino_vigencia": 1286164800000,
                    "negocio_id": "RENPRIV",
                    "producto_id": "60001",
                    "estado_poliza_id": "4",
                    "sucursal_venta_id": "51",
                    "sucursal_mantencion_id": null,
                    "forma_pago_id": null
                };
            } else if (numero_poliza === "9-1590300") {
                examples['application/json'] = {
                    "numero_poliza": "9-1590300",
                    "inicio_vigencia": 902462400000,
                    "termino_vigencia": 925444800000,
                    "negocio_id": "SEGIND",
                    "producto_id": "200",
                    "estado_poliza_id": "401",
                    "sucursal_venta_id": "131",
                    "sucursal_mantencion_id": null,
                    "forma_pago_id": "3"
                }
            }
        }
        if (Object.keys(examples).length > 0) {
            resolve(examples[Object.keys(examples)[0]]);
        } else {
            resolve();
        }
    });
}


/**
 * obtener Polizas de persona segun rut indicado
 *
 * rut Integer rut
 * returns List
 **/
exports.obtenerPolizasPersonaUsingGET = function (rut) {
    return new Promise(function (resolve, reject) {
        var examples = {};
        if (rut === 1) {
            examples['application/json'] = [
                {
                    "numero_poliza": "15607",
                    "inicio_vigencia": null,
                    "termino_vigencia": 1301540400000,
                    "negocio_id": "CREIND",
                    "producto_id": "20050",
                    "estado_poliza_id": "503",
                    "sucursal_venta_id": "81",
                    "sucursal_mantencion_id": null,
                    "forma_pago_id": null
                },
                {
                    "numero_poliza": "18710",
                    "inicio_vigencia": null,
                    "termino_vigencia": 1180584000000,
                    "negocio_id": "CREIND",
                    "producto_id": "20050",
                    "estado_poliza_id": "503",
                    "sucursal_venta_id": "306",
                    "sucursal_mantencion_id": null,
                    "forma_pago_id": null
                },
                {
                    "numero_poliza": "8440",
                    "inicio_vigencia": null,
                    "termino_vigencia": 1254283200000,
                    "negocio_id": "CREIND",
                    "producto_id": "20050",
                    "estado_poliza_id": "503",
                    "sucursal_venta_id": "133",
                    "sucursal_mantencion_id": null,
                    "forma_pago_id": null
                },
                {
                    "numero_poliza": "32026",
                    "inicio_vigencia": null,
                    "termino_vigencia": 1314759600000,
                    "negocio_id": "CREIND",
                    "producto_id": "20050",
                    "estado_poliza_id": "503",
                    "sucursal_venta_id": "133",
                    "sucursal_mantencion_id": null,
                    "forma_pago_id": null
                },
                {
                    "numero_poliza": "31241",
                    "inicio_vigencia": null,
                    "termino_vigencia": 1212206400000,
                    "negocio_id": "CREIND",
                    "producto_id": "20050",
                    "estado_poliza_id": "503",
                    "sucursal_venta_id": "307",
                    "sucursal_mantencion_id": null,
                    "forma_pago_id": null
                },
                {
                    "numero_poliza": "31327",
                    "inicio_vigencia": null,
                    "termino_vigencia": 1314759600000,
                    "negocio_id": "CREIND",
                    "producto_id": "20050",
                    "estado_poliza_id": "503",
                    "sucursal_venta_id": "133",
                    "sucursal_mantencion_id": null,
                    "forma_pago_id": null
                },
                {
                    "numero_poliza": "18576",
                    "inicio_vigencia": null,
                    "termino_vigencia": 1180584000000,
                    "negocio_id": "CREIND",
                    "producto_id": "20050",
                    "estado_poliza_id": "503",
                    "sucursal_venta_id": "307",
                    "sucursal_mantencion_id": null,
                    "forma_pago_id": null
                },
            ];
        } else if (rut === 2665352) {
            examples['application/json'] = [
                {
                    "inicio_vigencia": 902462400000,
                    "tipo_negocio_id": "SEGIND",
                    "tipo_producto_id": "200",
                    "tipo_rol_id": "1"
                },
                {
                    "inicio_vigencia": null,
                    "tipo_negocio_id": "RENPRIV",
                    "tipo_producto_id": "60001",
                    "tipo_rol_id": "5004"
                },
                {
                    "inicio_vigencia": 902462400000,
                    "tipo_negocio_id": "SEGIND",
                    "tipo_producto_id": "200",
                    "tipo_rol_id": "2"
                }
            ]
        }
        if (Object.keys(examples).length > 0) {
            resolve(examples[Object.keys(examples)[0]]);
        } else {
            resolve();
        }
    });
}

